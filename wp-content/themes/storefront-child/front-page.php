<?php
/**
 * Front page (Home page) tempalte file
 *
 * This file design to satisfy the design layout and structure. Split the layout in
 * multiple partials to re-use the code in other pages if needed.
 *
 * File does not support side bar feature as per the design
 *
 * @since   1.0.0
 * @package storefront
 */

// Get the header
get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

			<?php
			// Call to action
			get_template_part( 'partials/home-page/home-page.call-to-action' );

			// Featured products
			get_template_part( 'partials/home-page/home-page.feature-products' );

			// Blog section
			get_template_part( 'partials/home-page/home-page.blog-section' );

			// Recently added products
			get_template_part( 'partials/home-page/home-page.recently-added-products' );

			// List all the products parent category
			get_template_part( 'partials/home-page/home-page.product-category' );

			// Brands
			get_template_part( 'partials/home-page/home-page.brand-section' );

			?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
// Get the footer
get_footer();
