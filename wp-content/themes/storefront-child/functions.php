<?php
/**
 * This file will hook the functions to WordPress actions and filters.
 *
 * @since   1.0.0
 * @package storefront
 */

// File defines the ACF fields for CPT `homepage`
require_once dirname( __FILE__ ) . '/functions.acf.homepage.php';

/**
 * Function to return the fetured products.
 *
 * @since   1.0.0
 * @package storefront
 */
function pw_featured_products( $products ) {

	// Tax query
	$tax_query[] = array(
		'taxonomy' => 'product_visibility',
		'field'    => 'name',
		'terms'    => 'featured',
		'operator' => 'IN',
	);

	// The query
	return new WP_Query( array(
		'post_type'           => 'product',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
		'posts_per_page'      => $products,
		'order'               => 'desc',
		'tax_query'           => $tax_query
	) );

}

/**
 * Function to return recently added products.
 *
 * @since   1.0.0
 * @package storefront
 */
function pw_recently_added_products( $products ) {

	return new WP_Query( array(
		'post_type'      => 'product',
		'posts_per_page' => $products,
		'orderby'        => 'date',
		'order'          => 'DESC',
	) );
}

/**
 * Function to return short description
 *
 * @since   1.0.0
 * @package storefront
 */
function pw_custom_product_excerpt() {
	$excerpt = get_the_excerpt();
	$excerpt = preg_replace( " ([.*?])", '', $excerpt );
	$excerpt = strip_shortcodes( $excerpt );
	$excerpt = strip_tags( $excerpt );
	$excerpt = substr( $excerpt, 0, 150 );
	$excerpt = substr( $excerpt, 0, strripos( $excerpt, " " ) );
	$excerpt = trim( preg_replace( '/s+/', ' ', $excerpt ) );

	return $excerpt;
}

/**
 * Function to return all the product parent category
 *
 * @since   1.0.0
 * @package storefront
 */
function pw_get_parent_product_category() {
	$args = array(
		'taxonomy'   => 'product_cat',
		'orderby'    => 'name',
		'hide_empty' => '1'
	);

	return get_categories( $args );
}

/**
 * Add custom taxonomie for brand
 *
 * @since   1.0.0
 * @package storefront
 */
function pw_add_brand_taxonomies() {
	register_taxonomy( 'product_brand', 'product', array(
		'hierarchical' => true,
		'labels'       => array(
			'name'              => _x( 'Brands', 'brands' ),
			'singular_name'     => _x( 'Brand', 'brand' ),
			'search_items'      => __( 'Search Brands' ),
			'all_items'         => __( 'All Brands' ),
			'parent_item'       => __( 'Parent Brand' ),
			'parent_item_colon' => __( 'Parent Brand:' ),
			'edit_item'         => __( 'Edit Brand' ),
			'update_item'       => __( 'Update Brand' ),
			'add_new_item'      => __( 'Add New Brand' ),
			'new_item_name'     => __( 'New Brand Name' ),
			'menu_name'         => __( 'Brands' ),
		),
		'rewrite'      => array(
			'slug'         => 'brand',
			'with_front'   => false,
			'hierarchical' => true
		),
	) );
}

add_action( 'init', 'pw_add_brand_taxonomies', 0 );

/**
 * Function to return all the products brand
 *
 * @since   1.0.0
 * @package storefront
 */
function pw_get_parent_product_brand() {
	$args = array(
		'taxonomy'   => 'product_brand',
		'orderby'    => 'name',
		'hide_empty' => '1'
	);

	return get_categories( $args );
}
