<?php
// Get the all the product category
$pw_get_parent_product_brand = pw_get_parent_product_brand();

// Bail out if product category is not set
if ( empty( $pw_get_parent_product_brand ) ) {
	return;
}
?>
<section class="pw-product__brand"> <?php
	foreach ( $pw_get_parent_product_brand as $brand ) { ?>
        <!-- Brand title -->
        <h3><?php echo $brand->name; ?></h3>
		<?php
	} ?>
</section>
