<?php

// Get the all the featured products
$pw_featured_products = pw_featured_products( '2' );

// Bail out if feature product is not set
if ( ! $pw_featured_products->have_posts() ) {
	return;
}
?>
<section class="pw-action__feature_product"> <?php
	while ( $pw_featured_products->have_posts() ) : $pw_featured_products->the_post(); ?>
        <div class="pw-action__feature_product_main">

            <div class="pw-action__feature_product_main_image">
				<?php
				if ( has_post_thumbnail( $pw_featured_products->post->ID ) ) {
					echo get_the_post_thumbnail( $pw_featured_products->post->ID, 'thumbnail' );
				} else {
					echo '<img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" width="65px" height="115px" />';
				}
				?>
            </div>

            <!-- Product title -->
            <h3><?php the_title(); ?></h3>

            <!-- Product Custom button -->
            <div class="pw-action__feature_product_main_button">
                <a href="<?php echo esc_url( get_permalink( $pw_featured_products->post->ID ) ); ?>"
                   class="btn btn--big">Compare</a>
            </div>
        </div>
	<?php endwhile;
	wp_reset_query(); ?>
</section>
