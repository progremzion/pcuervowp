<?php
// Get the all the recently added products
$pw_recently_added_products = pw_recently_added_products( '4' );

// Bail out if there are no products
if ( ! $pw_recently_added_products->have_posts() ) {
	return;
}
?>
<section class="pw-recently__added_products">
    <div class="pw-recently__added_products_main"> <?php
		while ( $pw_recently_added_products->have_posts() ) : $pw_recently_added_products->the_post();
			global $product; ?>
            <div class="pw-recently__added_products_image">
				<?php
				if ( has_post_thumbnail( $pw_recently_added_products->post->ID ) ) {
					echo get_the_post_thumbnail( $pw_recently_added_products->post->ID, 'large' );
				} else {
					echo '<img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" width="65px" height="115px" />';
				}
				?>
            </div>

            <!-- Product title -->
            <h3>
                <a href="<?php echo esc_url( get_permalink( $pw_recently_added_products->post->ID ) ); ?>"><?php the_title(); ?></a>
            </h3>

            <!-- Product short description -->
			<?php echo pw_custom_product_excerpt(); ?>

            <!-- Display qty input -->
			<?php echo woocommerce_quantity_input( array(), $product, false ); ?>

            <!-- Product price -->
            <span class="price">
                <?php echo $product->get_price_html(); ?>
            </span>

            <!-- Product add to cart button -->
			<?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>

		<?php endwhile;
		wp_reset_query(); ?>
    </div>
</section>
