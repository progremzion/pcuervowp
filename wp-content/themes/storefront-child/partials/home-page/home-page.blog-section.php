<?php

// Bail out if one of the field is not set
if ( empty( get_field( 'blog_img' ) ) || empty( get_field( 'blog_button_title' ) ) || empty( get_field( 'blog_button_link' ) ) ) {
	return;
}

// Background image
$pw_blog_background_url = get_field( 'blog_img' );
?>
<section class="pw-blog__section"
         style="background-image: url(<?php echo esc_url( $pw_blog_background_url['url'] ); ?>)">
    <div class="pw-blog__section_button">
        <a href="<?php echo esc_url( get_field( 'blog_button_link' ) ); ?>"
           class="btn btn--big"><?php echo esc_html( get_field( 'blog_button_title' ) ); ?></a>
    </div>
</section>
