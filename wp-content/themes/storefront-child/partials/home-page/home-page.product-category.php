<?php
// Get the all the product category
$pw_get_parent_product_category = pw_get_parent_product_category();

// Bail out if product category is not set
if ( empty( $pw_get_parent_product_category ) ) {
	return;
}
?>
<section class="pw-product__category"> <?php
	foreach ( $pw_get_parent_product_category as $cat ) {

		$cat_thumb_id     = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
		$shop_catalog_img = wp_get_attachment_image_src( $cat_thumb_id, 'shop_catalog' );
		$category_id      = $cat->term_id;

		if ( $shop_catalog_img[0] ) {
			echo '<img src="' . $shop_catalog_img[0] . '" alt="' . $cat->name . '" />';
		} else {
			echo '<img src="' . woocommerce_placeholder_img_src( 'shop_catalog' ) . '" alt="Placeholder" width="65px" height="115px" />';
		}

		// Category title
		echo '<h3>' . $cat->name . '</h3>';

		// Category short description
		echo '<span class="pw-product__category_desc">' . $cat->description . '</span>'; ?>

        <div class="pw-product__category_button">
            <a href="<?php echo esc_url( get_term_link( $cat->slug, 'product_cat' ) ); ?>" class="btn btn--big">IR A
                CATEGORÍA</a>
        </div> <?php

	} ?>
</section>