<?php

// Bail out if one of the field is not set
if ( empty( get_field( 'action_title' ) ) || empty( get_field( 'action_image' ) ) || empty( get_field( 'button_title' ) ) || empty( get_field( 'action_link' ) ) ) {
	return;
}

// Background image
$pw_action_background_url = get_field( 'action_image' );
?>
<section class="pw-action__section"
         style="background-image: url(<?php echo esc_url( $pw_action_background_url['url'] ); ?>)">
    <div class="pw-action__section_title">
        <h2><?php echo esc_html( get_field( 'action_title' ) ); ?></h2>
    </div>

    <div class="pw-action__section_button">
        <a href="<?php echo esc_url( get_field( 'action_link' ) ); ?>"
           class="btn btn--big"><?php echo esc_html( get_field( 'button_title' ) ); ?></a>
    </div>
</section>
